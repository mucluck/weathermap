import React, { Component } from "react";
import {
  Typography,
  withStyles,
  Card,
  CardContent,
  CircularProgress
} from "@material-ui/core/";
import { connect } from "react-redux";
import _ from "lodash";

const styles = theme => ({
  card: {
    minHeight: 200,
    position: "relative",
    display: "flex",
    justifyContent: "center"
  },
  content: {},
  loader: {
    position: "absolute",
    left: 0,
    right: 0,
    margin: "auto",
    top: 0,
    bottom: 0
  },
  visual: {
    display: "flex",
    alignItems: "center"
  },
  description: {
    marginLeft: 8
  }
});

class UnmappedCurrent extends Component {
  render() {
    return (
      <Card className={this.props.classes.card}>
        {this.props.loaded ? (
          <CardContent>
            <Typography
              className={this.props.classes.city}
              variant="h5"
              component="h2"
            >
              {`${_.get(this.props, "currentlyWeatherData.name", "")}, ${_.get(
                this.props,
                "currentlyWeatherData.sys.country",
                ""
              )}`}
            </Typography>
            <Typography component="div" className={this.props.classes.visual}>
              <img
                src={`http://openweathermap.org/img/w/${_.get(
                  this.props,
                  "currentlyWeatherData.weather[0].icon",
                  "10n"
                )}.png`}
                alt={`${_.get(
                  this.props,
                  "currentlyWeatherData.weather[0].main",
                  "icon"
                )}`}
              />
              <Typography className={this.props.classes.description}>
                {`${_.get(
                  this.props,
                  "currentlyWeatherData.weather[0].description",
                  ""
                )}`}
              </Typography>
            </Typography>
            <Typography
              className={this.props.classes.title}
              color="textSecondary"
            >
              Temperature:
              {` ${~~_.get(this.props, "currentlyWeatherData.main.temp", "")}°`}
            </Typography>
            <Typography
              className={this.props.classes.title}
              color="textSecondary"
            >
              Wind:
              {` ${_.get(
                this.props,
                "currentlyWeatherData.wind.speed",
                ""
              )} m/s`}
            </Typography>
            <Typography
              className={this.props.classes.title}
              color="textSecondary"
            >
              Visibility:
              {` ${_.get(this.props, "currentlyWeatherData.visibility", 0) /
                100}%`}
            </Typography>
            <Typography
              className={this.props.classes.title}
              color="textSecondary"
            >
              Humidity:
              {` ${_.get(
                this.props,
                "currentlyWeatherData.main.humidity",
                ""
              )}%`}
            </Typography>
          </CardContent>
        ) : (
          <CircularProgress
            className={this.props.classes.loader}
            disableShrink
          />
        )}
      </Card>
    );
  }
}

export default connect((state, ownProps) => {
  return {
    currentlyWeatherData: state.currentlyWeatherData,
    requestCurrentlyLocationData: state.requestCurrentlyLocationData,
    loaded: state.loaded,
    state: state,
    ...ownProps
  };
})(withStyles(styles)(UnmappedCurrent));
