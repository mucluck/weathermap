import React, { Component } from "react";
import {
  Typography,
  Card,
  CardContent,
  CircularProgress,
  withStyles
} from "@material-ui/core/";
import { connect } from "react-redux";
import { store, actions } from "../store/index";
import _ from "lodash";

const styles = theme => ({
  card: {
    minHeight: 200,
    position: "relative",
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "flex-start"
  },
  content: {
    flex: "1 1 20%"
  },
  loader: {
    position: "absolute",
    left: 0,
    right: 0,
    margin: "auto",
    top: 0,
    bottom: 0
  },
  visual: {
    display: "flex",
    alignItems: "center"
  },
  description: {
    marginLeft: 8
  }
});

class UnmappedForecast extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: this.props.data || null,
      forecast: []
    };
  }

  componentDidMount() {
    this.props.requestForecastLocationData && store.dispatch(actions.getForecastLocationData(store.getState().coords));
  }

  render() {
    return (
      <Card className={this.props.classes.card}>
        {this.props.requestForecastLocationData ? (
          _.get(this.props, "currentlyForecastData.list", []).map((weather, idx) => (
            <CardContent
              className={this.props.classes.content}
              key={`card-content-${idx}`}
            >
              <Typography
                className={this.props.classes.city}
                variant="h5"
                component="h2"
              >
                {`${_.get(weather, "dt_txt", "").split(" ")[0]}`}
              </Typography>
              <Typography component="div" className={this.props.classes.visual}>
                <img
                  src={`http://openweathermap.org/img/w/${_.get(
                    weather,
                    "weather[0].icon",
                    "10n"
                  )}.png`}
                  alt={`${_.get(weather, "weather[0].main", "icon")}`}
                />
                <Typography className={this.props.classes.description}>
                  {`${_.get(weather, "weather[0].description", "")}`}
                </Typography>
              </Typography>
              <Typography
                className={this.props.classes.title}
                color="textSecondary"
              >
                Temperature:
                {` ${~~_.get(weather, "main.temp", "")}°`}
              </Typography>
              <Typography
                className={this.props.classes.title}
                color="textSecondary"
              >
                Wind:
                {` ${_.get(weather, "wind.speed", "")} m/s`}
              </Typography>
              <Typography
                className={this.props.classes.title}
                color="textSecondary"
              >
                Visibility:
                {` ${_.get(weather, "visibility", 0) / 100}%`}
              </Typography>
              <Typography
                className={this.props.classes.title}
                color="textSecondary"
              >
                Humidity:
                {` ${_.get(weather, "main.humidity", "")}%`}
              </Typography>
            </CardContent>
          ))
        ) : (
          <CircularProgress
            className={this.props.classes.loader}
            disableShrink
          />
        )}
      </Card>
    );
  }
}

export default connect((state, ownProps) => {
  return {
    currentlyForecastData: state.currentlyForecastData,
    requestForecastLocationData: state.requestForecastLocationData,
    state: state,
    ...ownProps
  };
})(withStyles(styles)(UnmappedForecast));
