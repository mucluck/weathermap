import { call, put, takeEvery } from "redux-saga/effects";

function* fetchWeatherDataByCoordsAsync(reduxData) {
  yield put({
    type: "REQUEST_CURRENTLY_LOCATION_DATA",
    requestCurrentlyLocationData: true,
    coords: reduxData.coords
  });

  try {
    const currentlyWeatherData = yield call(() => {
      return fetch("http://localhost:3003/weather", {
        method: "POST",
        mode: "cors",
        cache: "default",
        body: JSON.stringify(reduxData.coords),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      }).then(data => data.json());
    });

    yield put({
      type: "CURRENTLY_WEATHER_DATA",
      currentlyWeatherData: currentlyWeatherData,
      requestCurrentlyLocationData: false,
      loaded: true
    });
  } catch (event) {
    yield put({ type: "FETCH_FAILED", message: event.message });
  }
}

function* watchFetchWeatherDataByCoords() {
  yield takeEvery("GET_CURRENTLY_LOCATION_DATA", fetchWeatherDataByCoordsAsync);
}

function* fetchForecastDataByCoordsAsync(reduxData) {
  yield put({
    type: "REQUEST_FORECAST_LOCATION_DATA",
    requestForecastLocationData: true
  });

  try {
    const currentlyForecastData = yield call(() => {
      return fetch("http://localhost:3003/forecast", {
        method: "POST",
        mode: "cors",
        cache: "default",
        body: JSON.stringify(reduxData.coords),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      }).then(data => data.json());
    });

    yield put({
      type: "CURRENTLY_FORECAST_DATA",
      currentlyForecastData: currentlyForecastData,
      requestForecastLocationData: false,
    });
  } catch (event) {
    yield put({ type: "FETCH_FAILED", message: event.message });
  }
}

function* watchFetchForecastDataByCoords() {
  yield takeEvery(
    "GET_FORECAST_LOCATION_DATA",
    fetchForecastDataByCoordsAsync
  );
}

export { watchFetchWeatherDataByCoords, watchFetchForecastDataByCoords };
