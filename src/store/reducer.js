import constants from "./constants";

const initialState = {
  currentlyWeatherData: {},
  currentlyForecastData: {},
  loaded: false,
  coords: { longitude: 55.75222, latitude: 37.61556 },
  requestForecastLocationData: false
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case constants.REQUEST_CURRENTLY_LOCATION_DATA:
      return {
        ...state,
        requestCurrentlyLocationData: action.requestCurrentlyLocationData,
        coords: action.coords
      };
    case constants.REQUEST_FORECAST_LOCATION_DATA:
      return {
        ...state,
        requestForecastLocationData: action.requestForecastLocationData
      };
    case constants.CURRENTLY_WEATHER_DATA:
      return {
        ...state,
        currentlyWeatherData: action.currentlyWeatherData,
        requestCurrentlyLocationData: action.requestCurrentlyLocationData,
        loaded: action.loaded
      };
    case constants.CURRENTLY_FORECAST_DATA:
      return {
        ...state,
        currentlyForecastData: action.currentlyForecastData,
        requestCurrentlyLocationData: action.requestCurrentlyLocationData,
        requestForecastLocationData: true
      };
    case constants.GET_CURRENTLY_LOCATION_DATA:
      return {
        ...state,
        coords: action.coords
      };
    case constants.GET_FORECAST_LOCATION_DATA:
      return {
        ...state,
        coords: action.coords
      };

    default:
      return state;
  }
};

export default rootReducer;
