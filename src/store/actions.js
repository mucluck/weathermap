import constants from "./constants";

export default {
  requestCurrentlyLocationData: function(coords) {
    return {
      type: constants.REQUEST_CURRENTLY_LOCATION_DATA,
      requestCurrentlyLocationData: true,
      coords: coords
    };
  },
  requestForecastLocationData: function() {
    return {
      type: constants.REQUEST_FORECAST_LOCATION_DATA,
      requestForecastyLocationData: true,
    };
  },
  currentlyWeatherData: function(
    currentlyWeatherData,
    requestCurrentlyLocationData,
    loaded
  ) {
    return {
      type: constants.CURRENTLY_WEATHER_DATA,
      currentlyWeatherData: currentlyWeatherData,
      requestCurrentlyLocationData: requestCurrentlyLocationData,
      loaded: loaded
    };
  },
  currentlyForecastData: function(
    currentlyForecastData,
    requestCurrentlyLocationData,
    loaded
  ) {
    return {
      type: constants.CURRENTLY_FORECAST_DATA,
      currentlyForecastData: currentlyForecastData,
      requestCurrentlyLocationData: requestCurrentlyLocationData,
      loaded: loaded
    };
  },
  getCurrentlyLocationData: function(coords) {
    return {
      type: constants.GET_CURRENTLY_LOCATION_DATA,
      coords: coords
    };
  },
  getForecastLocationData: function(coords) {
    return {
      type: constants.GET_FORECAST_LOCATION_DATA,
      coords: coords
    };
  }
};
