import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";

import reducer from "./reducer";
import {
  watchFetchWeatherDataByCoords,
  watchFetchForecastDataByCoords
} from "../saga/sagas";

const sagaMiddleware = createSagaMiddleware();

const thisStore = createStore(reducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(watchFetchWeatherDataByCoords);
sagaMiddleware.run(watchFetchForecastDataByCoords);

global.store = thisStore;

export default thisStore;
