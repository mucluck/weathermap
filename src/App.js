import React, { Component } from "react";
import { BrowserRouter } from "react-router-dom";
import { LocationOn, ViewHeadline } from "@material-ui/icons";

import Header from "./layout/Header";
import Current from "./Pages/Current";
import Forecast from "./Pages/Forecast";
import Router from "./layout/Router";

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pages: []
    };
  }

  get makePages() {
    return [
      {
        title: "Current",
        link: "/",
        icon: <LocationOn />,
        component: Current
      },
      {
        title: "Forecast",
        link: "/forecast",
        icon: <ViewHeadline />,
        component: Forecast
      }
    ];
  }

  render() {
    return (
      <BrowserRouter>
        <div
          style={{
            maxWidth: "800px",
            marginLeft: "auto",
            marginRight: "auto"
          }}
        >
          <Header title={"Weather"} items={this.makePages} />
          <Router pages={this.makePages} />
        </div>
      </BrowserRouter>
    );
  }
}
