import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { BottomNavigation, BottomNavigationAction } from "@material-ui/core";

export default class BottomNav extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: this.props.items || []
    };
  }

  render() {
    return (
      <BottomNavigation showLabels>
        {this.state.items.map((link, idx) => (
          <BottomNavigationAction component={NavLink} to={link.link} key={`navigation-item-${idx}`} label={link.title} icon={link.icon} />
        ))}
      </BottomNavigation>
    );
  }
}
