import React, { Component } from "react";
import { Button, Grid, TextField } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";

import { store, actions } from "../store/index";

const styles = theme => ({
  container: {
    display: "grid",
    gridTemplateColumns: "repeat(12, 1fr)",
    gridGap: `${theme.spacing.unit * 3}px`
  },
  paper: {
    padding: theme.spacing.unit,
    textAlign: "center",
    color: theme.palette.text.secondary,
    whiteSpace: "nowrap",
    marginBottom: theme.spacing.unit
  },
  divider: {
    margin: `${theme.spacing.unit * 2}px 0`
  }
});

class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isMount: false,
      coords: {},
      currentlyData: {}
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCurrentLocation = this.handleCurrentLocation.bind(this);
  }

  componentDidMount() {
    this.setState(
      {
        isMount: true
      },
    );
  }

  handleSubmit() {
    this.props.handler && this.props.handler(this);
  }

  handleCurrentLocation() {
    navigator.geolocation.getCurrentPosition(
      position =>
        this.setState(
          {
            coords: {
              longitude: position.coords.longitude,
              latitude: position.coords.latitude
            }
          },
          () => {
            fetch("http://localhost:3003/coords", {
              method: "POST",
              mode: "cors",
              cache: "default",
              body: JSON.stringify(this.state.coords),
              headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
              }
            })
              .then(data => data.json())
              .then(result =>
                this.setState(
                  {
                    currentlyData: result
                  },
                  () =>
                    store.dispatch(
                      actions.currentlyWeatherData(this.state.currentlyData)
                    )
                )
              );
          }
        ),
      error => console.log(error),
      {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      }
    );
  }

  render() {
    return (
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        spacing={24}
      >
        <Grid item xs={12}>
          <TextField
            id="location"
            label="Location"
            name="location"
            fullWidth
            margin="normal"
          />
        </Grid>
        <Grid item xs={6}>
          <Button variant="outlined" onClick={this.handleCurrentLocation}>
            My location
          </Button>
          <Button variant="outlined" color="primary">
            Search
          </Button>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(Main);
