import React, { Component, Fragment } from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  withStyles,
  InputBase,
  CircularProgress,
  Drawer,
  Divider,
  List,
  ListItem,
  ListItemText,
  ListItemIcon
} from "@material-ui/core/";
import { Menu, Search, GpsFixed, ChevronLeft } from "@material-ui/icons/";
import { fade } from "@material-ui/core/styles/colorManipulator";
import { green } from "@material-ui/core/colors/";
import _ from "lodash";

import { store, actions } from "../store/index";

const styles = theme => ({
  root: {
    width: "100%",
    marginBottom: 8
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block"
    }
  },
  temp: {
    marginLeft: 24
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing.unit,
      width: "auto"
    }
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  inputRoot: {
    color: "inherit",
    width: "100%"
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: 120,
      "&:focus": {
        width: 200
      }
    }
  },
  buttonProgress: {
    color: green[500],
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -17,
    marginLeft: -17
  },
  drawer: {
    width: 150,
    flexShrink: 0
  },
  drawerPaper: {
    width: 240
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: "0 8px",
    ...theme.mixins.toolbar,
    justifyContent: "flex-end"
  }
});

class UnMappedHeader extends Component {
  constructor(props) {
    super(props);

    this.state = {
      classes: this.props.classes,
      title: this.props.title,
      coords: {},
      isMount: false,
      isOpen: false,
      items: this.props.items || []
    };

    this.handleCurrentLocation = this.handleCurrentLocation.bind(this);
    this.handleMenuSwitch = this.handleMenuSwitch.bind(this);
  }

  componentDidMount() {
    this.setState({
      isMount: true
    });

    this.handleCurrentLocation();
  }

  handleMenuSwitch() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  handleCurrentLocation() {
    navigator.geolocation.getCurrentPosition(
      position =>
        this.setState(
          {
            coords: {
              longitude: position.coords.longitude,
              latitude: position.coords.latitude
            }
          },
          () => {
            store.dispatch(actions.getCurrentlyLocationData(this.state.coords));
            store.dispatch(actions.getForecastLocationData(this.state.coords));
          }
        ),
      error => {
        store.dispatch(
          actions.getCurrentlyLocationData(store.getState().coords)
        );
        console.log(error);
      },
      {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      }
    );
  }

  render() {
    return (
      <div className={this.state.classes.root}>
        <AppBar position="static">
          <Toolbar>
            <IconButton
              className={this.state.classes.menuButton}
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleMenuSwitch}
            >
              <Menu />
            </IconButton>
            {this.props.loaded && (
              <Fragment>
                <Typography variant="h6" color="inherit">
                  {`${_.get(this.props, "currentlyWeatherData.name", "")}`}
                </Typography>
                <Typography
                  className={this.state.classes.temp}
                  variant="h6"
                  color="inherit"
                >
                  {`${_.get(
                    this.props,
                    "currentlyWeatherData.main.temp",
                    "0"
                  )}°`}
                </Typography>
                <Typography className={this.state.classes.temp} />
                <img
                  src={`http://openweathermap.org/img/w/${_.get(
                    this.props,
                    "currentlyWeatherData.weather[0].icon",
                    "10n"
                  )}.png`}
                  alt={`${_.get(
                    this.props,
                    "currentlyWeatherData.weather[0].main",
                    "icon"
                  )}`}
                />
              </Fragment>
            )}

            <div className={this.state.classes.grow} />
            <div>
              <IconButton
                aria-haspopup="true"
                color="inherit"
                onClick={
                  !this.props.requestCurrentlyLocationData
                    ? this.handleCurrentLocation
                    : null
                }
              >
                <GpsFixed />
                {this.props.requestCurrentlyLocationData && (
                  <CircularProgress
                    size={34}
                    className={this.state.classes.buttonProgress}
                  />
                )}
              </IconButton>
            </div>
            <div className={this.state.classes.search}>
              <div className={this.state.classes.searchIcon}>
                <Search />
              </div>
              <InputBase
                placeholder="Search…"
                classes={{
                  root: this.state.classes.inputRoot,
                  input: this.state.classes.inputInput
                }}
              />
            </div>
          </Toolbar>
        </AppBar>
        <Drawer
          ref={menu => (this.menu = menu)}
          className={this.props.classes.drawer}
          variant="persistent"
          anchor="left"
          open={this.state.isOpen}
          classes={{
            paper: this.props.classes.drawerPaper
          }}
        >
          <div className={this.props.classes.drawerHeader}>
            <IconButton onClick={this.handleMenuSwitch}>
              <ChevronLeft />
            </IconButton>
          </div>
          <Divider />
          <List>
            {this.state.items.map((item, idx) => (
              <ListItem
                button
                key={`menu-item-${idx}`}
                onClick={this.handleMenuSwitch}
                component={NavLink}
                to={item.link}
              >
                <ListItemIcon>{item.icon}</ListItemIcon>
                <ListItemText primary={item.title} />
              </ListItem>
            ))}
          </List>
        </Drawer>
      </div>
    );
  }
}

export default connect((state, ownProps) => {
  return {
    requestCurrentlyLocationData: state.requestCurrentlyLocationData,
    currentlyWeatherData: state.currentlyWeatherData,
    loaded: state.loaded,
    state,
    ...ownProps
  };
})(withStyles(styles)(UnMappedHeader));
