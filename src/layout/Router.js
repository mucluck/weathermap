import React, { Component } from "react";
import uuid from "uuid";
import { Route } from "react-router-dom";

export default class Router extends Component {
  render() {
    return this.props.pages.map((page, idx) => (
      <Route
        key={uuid()}
        exact={!idx}
        path={page.link}
        component={page.component}
      />
    ));
  }
}
