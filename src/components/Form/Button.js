import React, { Component } from "react";

export default class Button extends Component {
  constructor(props) {
    super(props);

    this.state = {
      text: this.props.text || "<Button>"
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    if (this.props.handler) {
      event.preventDefault();
      event.stopPropagation();

      this.props.handler(this);
    }
  }

  render() {
    return <button onClick={this.handleClick}>{this.state.text}</button>;
  }
}
