import React, { Component, Fragment } from "react";
import uuid from "uuid";

export default class TextInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: this.props.id || uuid(),
      type: this.props.type || "text",
      name: this.props.name,
      label: this.props.label,
      value: this.props.value
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({
      value: event.target.value
    });
  }

  render() {
    return (
      <div className={"form-group"}>
        <input
          id={this.state.id}
          type={this.state.type}
          name={this.state.name}
          value={this.state.value}
          className={"form-control"}
          onChange={this.handleChange}
        />
      </div>
    );
  }
}
