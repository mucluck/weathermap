import React, { Component } from "react";

export default class Form extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event){
    event.preventDefault();
    
    this.props.handler && this.props.handler(this);
  }

  render() {
    return <form className={"w-100"} onSubmit={this.handleSubmit}>{this.props.children}</form>;
  }
}
