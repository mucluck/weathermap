module.exports = {
  access_token: "0c659d6f814cc5702f96f86983c27c21",
  apiUrlWeather: "https://api.openweathermap.org/data/2.5/weather",
  apiUrlForecast: "https://api.openweathermap.org/data/2.5/forecast",
  units: "metric",
  port: 3003
};
