const express = require("express");

const router = express.Router();

router.post("/weather", (request, response) => {
  ForecastApi.handleGetWeatherByCoordinates(request.body)
    .then(data => response.send(data.data))
    .catch(err => response.status(err.status).send(err.data));
});

router.post("/forecast", (request, response) => {
  ForecastApi.handleGetForecastByCoordinates(request.body)
    .then(data => response.send(data.data))
    .catch(err => response.status(err.status).send(err.data));
});

module.exports = router;
