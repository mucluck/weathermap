const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const config = require("./config");
const router = require("./routes");

global.ForecastApi = require("./api");

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.listen(config.port);

app.use("/", router);

module.exports = app;
