const config = require("../config");
const axios = require("axios");
const _ = require("lodash");

class ForecastApi {
  constructor(config) {
    this.config = config;
  }

  handleGetWeatherByCoordinates(coordinates = {}) {
    return new Promise((resolve, reject) => {
      this.handleRequest(
        `${_.get(this.config, "apiUrlWeather", "")}`,
        `lat=${coordinates.latitude}&lon=${coordinates.longitude}`
      )
        .then(response => resolve(response))
        .catch(error => reject(error));
    });
  }

  handleGetForecastByCoordinates(coordinates = {}) {
    return new Promise((resolve, reject) => {
      this.handleRequest(
        `${_.get(this.config, "apiUrlForecast", "")}`,
        `lat=${coordinates.latitude}&lon=${coordinates.longitude}`
      )
        .then(response => resolve(response))
        .catch(error => reject(error));
    });
  }

  handleRequest(URL = "", request = "", method = "GET") {
    console.log(`${URL}?${request}&appid=${this.config.access_token}`);
    return axios({
      method: method,
      url: `${URL}?${request}&appid=${this.config.access_token}&units=${this.config.units}`
    });
  }
}

module.exports = new ForecastApi(config);
